Cours link: [Courssera ](https://www.coursera.org/learn/linux-server-management-security/home/welcome)

<details><summary>How to install Linux</summary>

[**CentOS installation guide** ](https://www.howtoforge.com/tutorial/centos-7-minimal-server/)


[**Ubuntu installation guide** ](https://www.ubuntu.com/download/desktop/install-ubuntu-desktop)

</details>



<details><summary>Networking within Linux</summary>


[Recours au commandes basics ](https://gitlab.com/parfaittolefo23/my_cours_and_notes/-/blob/main/Basic_Linux_Commands.md)
</details>

<details><summary>Ajouter un utilisateur</summary>

`user add user_name`

Définissez un mot de pass

`passwd username`

Vérifier le fichier `/etc/passwd`  et `/etc/shadow`

Pour faire mieux creez un dossier pour le home de l'utilisteur, en root

`mkdir user_home_name`

Definissez le propriétaire du dossier

`chown user_name  user_home_name`

Modifier le home par defaut dans /etc/shadow

`vipw`

Editer et parametré d'autres paramettre par defaut

`vi /etc/adduser.conf`
</details>

<details><summary>Assurer la sécurité de l'utilisateur</summary>

Pour faire mieux creez un dossier pour le home de l'utilisteur, en root

`mkdir user_home_name`

Definissez le propriétaire du dossier

`chown user_name  user_home_name`

Modifier le home par defaut dans /etc/shadow

`vipw`

Editer et parametré d'autres paramettre par defaut

`vi /etc/adduser.conf`

<details><summary>A quoi consite la sécurité des utilisateurs</summary>

Sécuriser les utilisateurs garantit non seulement que nous sécurisons un utilisateur en s'assurant qu'il possède les fichiers dont il a besoin et en verrouillant les autres qui pourraient avoir accès à ces fichiers, mais aussi en modifiant les groupes afin que nous puissions laisser d'autres utilisateurs apporter des modifications aux répertoires d'autres utilisateurs, ou s'ils partagent des fichiers, afin de verrouiller certaines personnes d'autres zones dans les fichiers d'autres personnes.Pour ce faire, je devrais modifier le fichier de groupe etc pour permettre à d'autres utilisateurs d'être dans ce groupe. En plus d'ajouter des utilisateurs et de s'assurer que leurs répertoires personnels sont corrects et que leurs scripts shell sont corrects dans le fichier de mot de passe etc.
</details>

<details><summary>Supprimer un utilisateur qui part...</summary>

`userdel user_name`

Supprimer tous ceux sur quoi l'utilisteur a accès ou supprimer ces accès

Supprimer son /home/...

`ll /home/`
`sudo rm -rf user_home`

Rassurez vous d'avoir supprimer tout cron le concernant et autres fichiers

`find / -xdev -nouser`
</details>
</details>
<details><summary>Configuration du fichier `/etc/sudoers` pour les droits</summary>

Exemple standard:

`%sudo  ALL=(ALL:ALL)  ALL`

 1 --> Utilisateur / groupe
 
 2 --> Hôte

 3 --> Utilisateurs dont on veut délégué les droits

 4 --> Groupe on veut délégué les droits
 
 5 --> Commandes à éxécutée

➡  Utilisateur / groupe : pour spécifier un utilisateur, on indique tout simplement son identifiant, tandis que s'il s'agit d'un groupe on ajoute le préfixe "%". On spécifie un seul utilisateur ou groupe par ligne !

➡ Hôte : le fait d'indiquer ALL permet de dire que cette règle s'applique à toutes les machines

➡ Tous les utilisateurs : permet de spécifier l'utilisateur dont on prend les droits, avec "ALL" on englobe tous les utilisateurs y compris l'utilisateur "root" (ce qui nous intéresse)

➡ Tous les groupes : idem que pour les utilisateurs

➡ Toutes les commandes : permets de spécifier la ou les commandes que l'utilisateur (ou le groupe) spécifié en début de ligne peut exécuter sur la machine. S'il y a plusieurs commandes, il faut utiliser la virgule comme séparateur.

Note : ce qui est intéressant, c'est de donner des droits directement sur des groupes et ensuite d'ajouter le(s) utilisateur(s) à ce groupe.

Sources: [it-connect](https://www.it-connect.fr/commande-sudo-comment-configurer-sudoers-sous-linux/)

</details>

<details><summary>Authentication - PAM</summary>

Ressources: [Cliquez ici](https://www.youtube.com/watch?v=B9IG-5dKc1Y)

</details>

<details><summary>Using PAM
</summary>

[Cliquez ici](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Managing_Smart_Cards/Pluggable_Authentication_Modules.html)
</details>

<details><summary>Craker un mot de passe linux</summary>

On enrégistre toutes la ligne dans un fichier pas.txt, nous utilisons john comme suit:

`john --wordlist=/usr/share/wordlists/rockyou.txt pas.txt`

</details>


<details><summary>SELinux</summary>

**Discretionary Access Control ( D A C )**

Contrôle d'Accès Discrétionnaire

**Security Enhanced Linux ( SElINUX )**

Sécurité Renforcée Linux 



**Mandatory Access Control ( M A C )**

SELinux fonctionne en trois modes.

**Le mode application**

Elle implémente le Contrôle d'Accès Obligatoir sur toute la machine et donc les utilisateurs ne peuvent pas le contourner. Tout est enrégistré et tout est refusé par défaut.


**Le mode Permitif**

Il dit: permettre tout sauf enrégistrer

**Le mode désactivé**

Cliquez pour avoir le guide: [Documentation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/index)


</details>
